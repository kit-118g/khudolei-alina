﻿using System;

namespace khoudoley01
{
    class Program
    {
        static void Main(string[] args)
        {
            var record = new Student[3];
            bool loop = true;
            int index = 0;
            int size = 0;

            while (loop)
            {
                Console.WriteLine($"Добавить данные про студента?");
                string answer = Console.ReadLine();

                if (answer == "да" || answer == "Да" || answer == "Yes" || answer == "yes")//Equals
                {
                    record[index] = new Student();
                    Console.WriteLine($"Имя: ");
                    record[index].Name = Console.ReadLine();
                    Console.WriteLine($"Фамилия: ");
                    record[index].Lastname = Console.ReadLine();
                    Console.WriteLine($"Отчество: ");
                    record[index].Patronym = Console.ReadLine();
                    Console.WriteLine($"День рождения: ");
                    record[index].Birthday = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine($"Дата поступления: ");
                    record[index].Date = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine($"Индекс группы: ");
                    record[index].Index = Console.ReadLine();
                    Console.WriteLine($"Факультет: ");
                    record[index].Faculty = Console.ReadLine();
                    Console.WriteLine($"Специальность: ");
                    record[index].Specialty = Console.ReadLine();
                    Console.WriteLine($"Успеваемость: ");
                    record[index].Progress = int.Parse(Console.ReadLine());
                    index++;
                }
                else
                {
                    loop = false;
                    size = record.Length;

                    for (int i = 0; i < size; i++)
                    {
                        Console.WriteLine(record[i]);
                    }

                    Console.ReadKey();
                }
            }
        }
    }
}
