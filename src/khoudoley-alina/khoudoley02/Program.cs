﻿using khoudoley02;
using System;
using System.Collections.Generic;

namespace khoudoley02
{
    class Program
    {
        static void Main(string[] args)
        {
            var records = new Container();
            bool loop = true;
            string name, lastname, patronym, faculty, specialty;
            DateTime birthday, date;
            int progress, number;
            char index;
            string answer;
            while (loop)
            {
                Console.WriteLine($"Добавить данные про студента?");
                answer = Console.ReadLine();

                if (answer.Equals("да") || answer.Equals("Да") || answer.Equals("Yes") || answer.Equals("yes"))//Equals
                {
                    Console.WriteLine($"Имя: ");
                    name = Console.ReadLine();
                    Console.WriteLine($"Фамилия: ");
                    lastname = Console.ReadLine();
                    Console.WriteLine($"Отчество: ");
                    patronym = Console.ReadLine();
                    Console.WriteLine($"День рождения: ");
                    birthday = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine($"Дата поступления: ");
                    date = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine($"Индекс группы: ");
                    index = char.Parse(Console.ReadLine());
                    Console.WriteLine($"Факультет: ");
                    faculty = Console.ReadLine();
                    Console.WriteLine($"Специальность: ");
                    specialty = Console.ReadLine();
                    Console.WriteLine($"Успеваемость: ");
                    progress = int.Parse(Console.ReadLine());
                    var stud = new Student(name, lastname, patronym, birthday, date, index, faculty, specialty, progress);
                    records.Add(stud);
                    //var stud = new T(name, lastname, patronym, birthday, date, index, faculty, specialty, progress);
                    //records.Add(stud);
                }
                else
                {
                    loop = false;

                    foreach (var student in records)
                    {
                        Console.WriteLine(student + " ");
                    }
                    Console.ReadKey();
                }
            }
            Console.WriteLine($"Найти элемент по индексу?");
            answer = Console.ReadLine();
            if (answer.Equals("да") || answer.Equals("Да") || answer.Equals("Yes") || answer.Equals("yes"))//Equals
            {
                Console.WriteLine($"Индекс: ");
                number = int.Parse(Console.ReadLine());
                records.Search(records, number);
            }
        }
    }
}