﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace khoudoley02
{
    public class Container : IEnumerable, IEnumerator
    {
        static int size = 1;
        Student[] students = new Student[size];
        //Student[] students2 = new Student[size];
        int index = -1;
        int currentSize = 0;

        // Реализуем интерфейс IEnumerable
        public IEnumerator GetEnumerator()
        {
            return this;
        }

        // Реализуем интерфейс IEnumerator
        public bool MoveNext()
        {
            if (index == students.Length - 1)
            {
                Reset();
                return false;
            }

            index++;
            return true;
        }

        public void Reset()
        {
            index = -1;
        }

        public object Current
        {
            get
            {
                return students[index];
            }
        }

        public void Add(Student student)
        {
            currentSize = NewSize();
            if (currentSize < size)
            {
                students[currentSize] = student;
            }

            else
            {
                size = NewSize() * 2;
                Array.Resize(ref students, size);
                students[size - 1] = student;
            }
        }

        int NewSize()
        {
            currentSize = 0;
            for (int i = 0; i < students.Length; i++)
            {
                if (students[i] != null)
                    currentSize++;
            }
            return currentSize;
        }

        public void Search(Container records, int number)
        {
            Console.Write(records.students[number - 1] + " ");
        }
        /*static int size = 1;
        public static Student[] mas = new Student[size];
        //Student current = mas.;
       
        public Container()
        {

        }
        private static void IterateThruContainer()
        {
            Container<khoudoley02.Student> records = BuildContainer();

            foreach (var student in records)
            {
                Console.WriteLine(student + " ");
            }
        }

        private static Container<khoudoley02.Student> BuildContainer()
        {
            var records = new Container<khoudoley02.Student>();

            //AddToDictionary(elements, "K", "Potassium", 19);
            //AddToDictionary(elements, "Ca", "Calcium", 20);
            //AddToDictionary(elements, "Sc", "Scandium", 21);
            //AddToDictionary(elements, "Ti", "Titanium", 22);

            return records;
        }

        private static void AddToRecords(Container<khoudoley02.Student> records, khoudoley02.Student student)
        {

        }

        public IEnumerator GetEnumerator()
        {
            return new Enumerator();
        }

        System.Collections.IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }*/
    }
}