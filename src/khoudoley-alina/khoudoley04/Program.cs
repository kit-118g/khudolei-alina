﻿using System;
using System.IO;
using System.Text;

namespace khoudoley04
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"D:\Университет\.NET\khoudoley\khudolei-alina\src\khoudoley-alina\khoudoley04\records.txt";
            StringBuilder sb = new StringBuilder();
            var records = new Container();
            bool loop = true;
            string name, lastname, patronym, faculty;
            DateTime birthday, date;
            int progress, number, specialty;
            char index;
            string text;
            int choice;

            while (loop)
            {
                Console.WriteLine("Что Вы хотите сделать?\n 1 - Добавить данные про студента\n 2 - Вывести на экран данные\n 3 - Записать данные в файл" +
                    "\n 4 - Прочитать данные из файла\n 5 - Найти элемент по индексу\n 6 - Удалить данные о студенте\n 7 - Редактировать данные студента" +
                    "\n 8 - Вывести название группы студента\n 9 - Вывести текущий курс и семестр студента\n 10 - Вывести текущий возраст студента\n" +
                    "11 - Выход");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Имя: ");
                        name = Console.ReadLine();
                        Console.WriteLine("Фамилия: ");
                        lastname = Console.ReadLine();
                        Console.WriteLine("Отчество: ");
                        patronym = Console.ReadLine();
                        Console.WriteLine("День рождения: ");
                        birthday = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Дата поступления: ");
                        date = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Индекс группы: ");
                        index = char.Parse(Console.ReadLine());
                        Console.WriteLine("Факультет: ");
                        faculty = Console.ReadLine();
                        Console.WriteLine("Специальность: ");
                        specialty = int.Parse(Console.ReadLine());
                        Console.WriteLine("Успеваемость: ");
                        progress = int.Parse(Console.ReadLine());
                        var stud = new Student(name, lastname, patronym, birthday, date, index, faculty, specialty, progress);
                        records.Add(stud);
                        break;
                    case 2:
                        foreach (var student in records)
                        {
                            Console.WriteLine(student + " ");
                        }
                        break;
                    case 3:
                        try
                        {
                            using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                            {
                                sw.WriteLine("Студенты: ");
                            }
                            foreach (var student in records)
                            {
                                using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
                                {
                                    sw.WriteLine(student);
                                }
                            }
                            Console.WriteLine("Запись выполнена");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 4:
                        try
                        {
                            using (StreamReader sr = new StreamReader(path))
                            {
                                text = sr.ReadToEnd();

                                string[] separatingStrings = { " ", "\r", "\n", ":", "00:00:00", "\t", "Студенты", "Имя", "Фамилия", "Отчество", "День рождения",
                                    "Дата поступления", "Индекс группы", "Факультет", "Специальность", "Успеваемость"};
                                string[] words = text.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);
                                for (int i = 0; i < words.Length / 9; i++)
                                {
                                    name = words[9 * i];
                                    lastname = words[9 * i + 1];
                                    patronym = words[9 * i + 2];
                                    birthday = DateTime.Parse(words[9 * i + 3]);
                                    date = DateTime.Parse(words[9 * i + 4]);
                                    index = char.Parse(words[9 * i + 5]);
                                    faculty = words[9 * i + 6];
                                    specialty = int.Parse(words[9 * i + 7]);
                                    progress = int.Parse(words[9 * i + 8]);
                                    stud = new Student(name, lastname, patronym, birthday, date, index, faculty, specialty, progress);
                                    records.Add(stud);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 5:
                        Console.WriteLine("Номер студента, которого хотите найти: ");
                        number = int.Parse(Console.ReadLine());
                        records.Search(records, number);
                        break;
                    case 6:
                        Console.WriteLine("Номер студента, данные о котором хотите удалить: ");
                        number = int.Parse(Console.ReadLine());
                        records.Remove(records, number);
                        break;
                    case 7:
                        Console.WriteLine("Номер студента, данные о котором хотите отредактировать: ");
                        number = int.Parse(Console.ReadLine());
                        Console.WriteLine("Что хотите отредактировать? (1-имя, 2 - фамилию, 3 - отчество, 4 - день рождения, 5 - дата поступления, " +
                                        "6 - индекс группы, 7 - факультет, 8 - специальность, 9 - успеваемость");
                        int n;
                        string str;
                        n = int.Parse(Console.ReadLine());
                        Console.WriteLine("Введите новые данные в соответствующем формате : ");
                        str = Console.ReadLine();
                        switch (n)
                        {
                            case 1:
                                records.Edit(records, number, "name", str);
                                break;
                            case 2:
                                records.Edit(records, number, "lastname", str);
                                break;
                            case 3:
                                records.Edit(records, number, "patronym", str);
                                break;
                            case 4:
                                records.Edit(records, number, "birthday", str);
                                break;
                            case 5:
                                records.Edit(records, number, "date", str);
                                break;
                            case 6:
                                records.Edit(records, number, "index", str);
                                break;
                            case 7:
                                records.Edit(records, number, "faculty", str);
                                break;
                            case 8:
                                records.Edit(records, number, "specialty", str);
                                break;
                            case 9:
                                records.Edit(records, number, "progress", str);
                                break;
                        }
                        break;
                    case 8:
                        Console.WriteLine("Номер студента, чью группу хотите узнать: ");
                        number = int.Parse(Console.ReadLine());
                        sb.Append(records.students[number - 1].Faculty);
                        sb.Append(records.students[number - 1].Specialty);
                        sb.Append("-");
                        sb.Append(records.students[number - 1].Date.Year);
                        sb.Append(records.students[number - 1].Index);
                        sb.AppendLine();
                        Console.WriteLine(sb.ToString());
                        break;
                    case 9:
                        Console.WriteLine("Номер студента, чей номер курса и семестра на текущий момент хотите узнать: ");
                        number = int.Parse(Console.ReadLine());
                        int course, semester;
                        course = DateTime.Today.Year - records.students[number - 1].Date.Year;
                        if (DateTime.Today.Month >= 7 && DateTime.Today.Month <= 12)
                        {
                            semester = course * 2 - 1;
                        }
                        else
                        {
                            semester = course * 2;
                        }
                        Console.WriteLine($"Курс : {course}, семестр : {semester}");
                        break;
                    case 10:
                        Console.WriteLine("Номер студента, чей текущий возраст хотите узнать: ");
                        number = int.Parse(Console.ReadLine());
                        DateTime today = DateTime.Today;
                        DateTime b = records.students[number - 1].Birthday;
                        TimeSpan old = today.Subtract(b);
                        var d = new DateTime(old.Ticks);
                        /*int year = Convert.ToInt32(Math.Floor(old.TotalDays / 365));
                        int month = Convert.ToInt32(Math.Floor((old.TotalDays % 365) / 31));
                        int day = Convert.ToInt32(Math.Floor((old.TotalDays % 365) % 31));*/
                        Console.WriteLine($"Возраст: {d.Year-1} лет, {d.Month-1} месяцев, {d.Day-1} дней");
                        break;
                    case 11:
                        loop = false;
                        break;
                }
            }
        }
    }
}