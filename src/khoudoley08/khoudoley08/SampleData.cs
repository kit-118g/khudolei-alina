﻿using System;
using System.Linq;
using khoudoley08.Models;

namespace khoudoley08
{
    public class SampleData
    {
        public static void Initialize(Context context)
        {
            if (!context.Students.Any())
            {
                context.Students.AddRange(
                    new Student
                    {
                        Name = "Марина",
                        Lastname = "Сергієнко",
                        Patronym = "Володимирівна",
                        Birthday = new DateTime(2000,07, 12),
                        Date = new DateTime(2017, 08, 22),
                        Index = 'а',
                        Faculty = "КІТ",
                        Specialty = 123,
                        Progress = 95
                    },
                    new Student
                    {
                        Name = "Олег",
                        Lastname = "Ткаченко",
                        Patronym = "Сергійович",
                        Birthday = new DateTime(2002, 11, 14),
                        Date = new DateTime(2019, 09, 3),
                        Index = 'в',
                        Faculty = "КІТ",
                        Specialty = 121,
                        Progress = 93
                    },
                    new Student
                    {
                        Name = "Ольга",
                        Lastname = "Кравченко",
                        Patronym = "Олександрівна",
                        Birthday = new DateTime(2001, 2, 20),
                        Date = new DateTime(2018, 09, 11),
                        Index = 'г',
                        Faculty = "СГТ",
                        Specialty = 93,
                        Progress = 97
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
