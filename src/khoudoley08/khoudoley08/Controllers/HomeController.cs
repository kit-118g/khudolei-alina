﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using khoudoley08.Models;

namespace khoudoley08.Controllers
{
    public class HomeController : Controller
    {
        Context db;
        public HomeController(Context context)
        {
            db = context;
        }
        public IActionResult Index()
        {
            return View(db.Students.ToList());
        }
    }
}
