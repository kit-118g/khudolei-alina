﻿using Microsoft.EntityFrameworkCore;

namespace khoudoley08.Models
{
    public class Context : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
